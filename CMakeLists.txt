cmake_minimum_required(VERSION 3.14)
project(window_preview C)

set(CMAKE_C_STANDARD 11)

find_package(PkgConfig)

pkg_check_modules (GLIB REQUIRED glib-2.0)
pkg_check_modules (GTK REQUIRED gtk+-3.0)
pkg_check_modules (WNCK REQUIRED libwnck-3.0)
pkg_check_modules (CAIRO_XLIB REQUIRED cairo-xlib)
pkg_check_modules (XCOMPOSITE xcomposite)

add_executable(window-preview "")

target_sources(window-preview PRIVATE
        main.c
        wnck-extension.c
        wnck-extension.h)

target_include_directories(window-preview PRIVATE
        ${GLIB_INCLUDE_DIRS}
        ${GTK_INCLUDE_DIRS}
        ${WNCK_INCLUDE_DIRS}
        ${CAIRO_XLIB_INCLUDE_DIRS}
        ${XCOMPOSITE_LIBRARIES})

target_link_libraries(window-preview
        ${GLIB_LIBRARIES}
        ${GTK_LIBRARIES}
        ${WNCK_LIBRARIES}
        ${CAIRO_XLIB_LIBRARIES}
        ${XCOMPOSITE_LIBRARIES})
