#include "wnck-extension.h"

#include <X11/extensions/Xcomposite.h>
#include <gdk/gdkx.h>
#include <cairo/cairo-xlib.h>

cairo_surface_t *
render_surface_image (cairo_surface_t *original_surface, int height, int width)
{
  cairo_surface_t *image_surface;
  cairo_t *cr;

  image_surface = cairo_surface_create_similar (original_surface,
                                                CAIRO_CONTENT_COLOR_ALPHA,
                                                width,
                                                height);

  cr = cairo_create (image_surface);
  cairo_set_source_surface (cr, original_surface, 0, 0);

  cairo_paint (cr);
  cairo_destroy (cr);

  return image_surface;
}

cairo_surface_t *
get_surface_for_pixmap (Pixmap   x_pixmap,
                        Display *x_display,
                        Window   x_window)
{
  cairo_surface_t *xlib_surface;
  cairo_surface_t *rendered_surface;

  XWindowAttributes window_attributes;

  Status x_status;
  Visual *x_visual;
  int height, width;

  x_status = XGetWindowAttributes (x_display, x_window, &window_attributes);

  if (x_status == 0)
    return NULL;

  x_visual = window_attributes.visual;

  height = window_attributes.height;
  width = window_attributes.width;

  xlib_surface = cairo_xlib_surface_create (x_display, x_pixmap, x_visual, width, height);

  rendered_surface = render_surface_image (xlib_surface, height, width);

  cairo_surface_destroy (xlib_surface);

  return rendered_surface;
}

cairo_surface_t *
wnck_window_get_window_picture (WnckWindow *window)
{
  Pixmap x_pixmap;
  Display *x_display;
  Window x_window;

  GdkDisplay *display;

  cairo_surface_t *surface;

  display = gdk_display_get_default ();
  x_display = gdk_x11_display_get_xdisplay (display);
  x_window = wnck_window_get_xid (window);

  gdk_x11_display_error_trap_push (display);

  //XSynchronize (x_display, 1);

  x_pixmap = XCompositeNameWindowPixmap (x_display, x_window);

  gdk_display_flush (display);
  if (gdk_x11_display_error_trap_pop (display))
  {
    x_pixmap = None;
  }

  if (x_pixmap == None || x_pixmap == BadMatch)
    return NULL;

  surface = get_surface_for_pixmap (x_pixmap, x_display, x_window);

  XFreePixmap (x_display, x_pixmap);

  return surface;
}
