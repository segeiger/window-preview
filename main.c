#include <stdio.h>

#include "wnck-extension.h"

#include <gtk/gtk.h>
#include <libwnck/libwnck.h>

static void
activate (GApplication *app,
          gpointer      user_data)
{
  GtkWidget *window = gtk_window_new (GTK_WINDOW_TOPLEVEL);

  gtk_application_add_window (GTK_APPLICATION (app), GTK_WINDOW (window));

  gtk_widget_show_all (window);
}

static void
print_window_names (GList *list)
{
  const char *name;
  int i;

  i = 0;

  while (list)
  {
    name = wnck_window_get_name (list->data);
    g_warning ("[%d] name: %s", i, name);
    list = list->next;
    i++;
  }
}

static void
window_addded (GtkApplication *application,
               GtkWindow      *window,
               gpointer        user_data)
{
  GtkWidget *image;
  GList *list;
  WnckScreen *wnck_screen;
  WnckWindow *wnck_window;
  cairo_surface_t *surface;

  wnck_screen = wnck_screen_get_default ();
  wnck_screen_force_update (wnck_screen);
  list = wnck_screen_get_windows (wnck_screen);

  print_window_names (list);

  wnck_window = g_list_nth_data (list, 17);

  surface = wnck_window_get_window_picture (wnck_window);

  image = gtk_image_new_from_surface (surface);

  gtk_container_add (GTK_CONTAINER (window), image);
}

int main (int argc, char **argv)
{
  GtkApplication * application;
  gint result;

  wnck_set_client_type (WNCK_TYPE_PAGER);

  application = gtk_application_new ("org.gnome.wnck.get-window-picture", G_APPLICATION_FLAGS_NONE);

  g_signal_connect (application, "activate", G_CALLBACK (activate), NULL);
  g_signal_connect (application, "window-added", G_CALLBACK (window_addded), NULL);

  result = g_application_run (G_APPLICATION (application), argc, argv);

  g_object_unref (application);

  return result;
}
