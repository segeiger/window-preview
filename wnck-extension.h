#define WNCK_I_KNOW_THIS_IS_UNSTABLE 1

#include <libwnck/libwnck.h>

cairo_surface_t *
wnck_window_get_window_picture (WnckWindow *window);
